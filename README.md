cd ~/code/devops-midwest
git clone git@gitlab.com:barkerd427/devops-midwest.git
curl -L https://git.io/getLatestIstio | sh -
export PATH="$PATH:~/code/devops-midwest/istio-1.1.1/bin"
brew install kubernetes-helm
brew upgrade kubernetes-helm


cd ~/code/devops-midwest/istio-1.1.1
kubectl apply -f install/kubernetes/helm/helm-service-account.yaml
helm init --service-account tiller
helm install install/kubernetes/helm/istio-init --name istio-init --namespace istio-system
k get crds | grep 'istio.io\|certmanager.k8s.io' | wc -l
helm install install/kubernetes/helm/istio --name istio --namespace istio-system --set tracing.enabled=true --set grafana.enabled=true --set servicegraph.enabled=true --set kiali.enabled=true
k get svc -n istio-system
k get pods -n istio-system
k create ns devops-midwest
k label ns devops-midwest istio-injection=enabled
k describe ns devops-midwest
k config set-context $(k config current-context) --namespace=devops-midwest
k apply -f samples/bookinfo/platform/kube/bookinfo.yaml
k apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
curl -o /dev/null -s -w "%{http_code}\n" http://127.0.0.1/productpage
k apply -f samples/bookinfo/networking/destination-rule-all-mtls.yaml
k get destinationrules -o yaml
cd ~/code/devops-midwest
k apply -f logging-stack.yaml
k apply -f fluentd-istio.yaml
for i in `seq 1 200`; do curl -o /dev/null -s -w "%{http_code}\n" http://127.0.0.1/productpage &; done

k -n istio-system port-forward $(kubectl -n istio-system get pod -l app=grafana -o jsonpath='{.items[0].metadata.name}') 3000:3000 &
k -n istio-system port-forward $(kubectl -n istio-system get pod -l app=jaeger -o jsonpath='{.items[0].metadata.name}') 16686:16686 &
k -n istio-system port-forward $(kubectl -n istio-system get pod -l app=prometheus -o jsonpath='{.items[0].metadata.name}') 9090:9090 &
k -n logging port-forward $(kubectl -n logging get pod -l app=kibana -o jsonpath='{.items[0].metadata.name}') 5601:5601 &

<!-- cd ~/code/devops-midwest
k apply -f new_telemetry.yaml
k describe prometheus doublehandler -n istio-system
curl http://127.0.0.1/productpage
k -n istio-system logs $(kubectl -n istio-system get pods -l istio-mixer-type=telemetry -o jsonpath='{.items[0].metadata.name}') -c mixer | grep \"instance\":\"newlog.logentry.istio-system\"
k apply -f tcp_telemetry.yaml
cd ~/code/devops-midwest/istio-1.1.1
k apply -f samples/bookinfo/platform/kube/bookinfo-ratings-v2.yaml
k apply -f samples/bookinfo/platform/kube/bookinfo-db.yaml
k apply -f samples/bookinfo/networking/destination-rule-all.yaml
k apply -f samples/bookinfo/networking/virtual-service-ratings-db.yaml -->
k apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml
k apply -f samples/bookinfo/networking/virtual-service-reviews-test-v2.yaml
# go to http://127.0.0.1/productpage
# hit refresh several times, you shouldn't see any star ratings
# go to the website and login with jason:jason
# hit refresh several times, you should see black star ratings
# Navigate to Prometheus http://127.0.0.1:9090, Grafana http://127.0.0.1:3000,
# Jaeger http://127.0.0.1:16686, and Kibana http://127.0.0.1:5601
# Now inject a delay
k apply -f samples/bookinfo/networking/virtual-service-ratings-test-delay.yaml
# This is only for jason
# Refresh the page
# Open developer tools
# Refresh
# The page loads in about 6 seconds, this is due to a timeout of 6s where we set a network delay of 7s.
# Navigate to Grafana, Jaeger, and Kibana
# Press sign out
# Reviews should reappear
# Inject an http abort for jason
k apply -f samples/bookinfo/networking/virtual-service-ratings-test-abort.yaml
# go back to the site and refresh

# Do a little traffic shifting
# Let's do a 50/50 split
k apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml
k apply -f samples/bookinfo/networking/virtual-service-reviews-50-v3.yaml
k get virtualservice reviews -o yaml
# Let's go all in with v3
k apply -f samples/bookinfo/networking/virtual-service-reviews-v3.yaml
k delete -f samples/bookinfo/networking/virtual-service-all-v1.yaml

for i in `seq 1 200`; do curl -o /dev/null -s -w "%{http_code}\n" http://127.0.0.1/productpage &; done

istio_requests_total{destination_service="productpage.devops-midwest.svc.cluster.local"}
istio_requests_total{destination_service="reviews.devops-midwest.svc.cluster.local", destination_version="v3"}
rate(istio_requests_total{destination_service=~"productpage.*", response_code="200"}[5m])

for i in `seq 1 200`; do curl -o /dev/null -s -w "%{http_code}\n" http://127.0.0.1/productpage &; done

# Delete all the things
cd ~/code/devops-midwest
k delete -f fluentd-istio.yaml
k delete -f logging-stack.yaml
cd ~/code/devops-midwest/istio-1.1.1
k delete -f samples/bookinfo/networking/virtual-service-ratings-db.yaml
k delete -f samples/bookinfo/networking/destination-rule-all.yaml
k delete -f samples/bookinfo/platform/kube/bookinfo-db.yaml
k delete -f samples/bookinfo/platform/kube/bookinfo-ratings-v2.yaml
cd ~/code/devops-midwest/
k delete -f tcp_telemetry.yaml
k delete -f new_telemetry.yaml
cd ~/code/devops-midwest/istio-1.1.1
k delete -f samples/bookinfo/networking/destination-rule-all-mtls.yaml
k delete -f samples/bookinfo/networking/bookinfo-gateway.yaml
k delete -f samples/bookinfo/platform/kube/bookinfo.yaml
k delete ns devops-midwest
helm del --purge istio
helm del --purge istio-init
helm reset
k delete -f install/kubernetes/helm/helm-service-account.yaml
k delete ns istio-system
